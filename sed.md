# super sweet sed fun time

## the awful truth about sed

It's probably one of the most misunderstood and purposefully avoided tools in the bash toolbox because it can seem like arcane witchcraft.  But!  i think it's one of the handiest tools.  Let's talk about some of the stuff I use `sed` for routinely.

## super basic example: replace something

`echo "hello replacement" | sed 's/replacement/world/`

something a little more practical? how about something like template replacement, like minstache?

`cat example1.txt | sed 's/{{name}}/dev ops dojo/'`

### a special note: delimiters

you can use anything you want so long as they match.  Yes, even dollarsigns.

### another special note about the previous special note

if you're using variable substitution in bash with double-quotes `"`, you'll have to escape the special characters.  Pro tip, don't use dollarsigns, as it makes things a mmmmess.

## extended regular expressions: extract something

you can also use `sed` to pull things out.  take extracting filenames:

a note: extended regex switches vary by flavor - `-r` for debian, `-E` for BSD and rhel

`find . -type f | sed -r 's/.*\/([a-zA-Z0-9]+)\.([a-zA-Z0-9]+)$/\1, \2/'`

ok, that's close, but it's still printing things that didn't match

`find . -type f | sed -n -E 's/.*\/([a-zA-Z0-9]+)\.([a-zA-Z0-9]+)$/\1, \2/p'`

`pwd | sed -r 's/.*\/([a-zA-Z0-9]+)\.?([a-zA-Z0-9]*)$/\1/'`

a sidebar about basename and dirname

## regexes: POSIX equivalents to PCRE character classes

posix regular expression classes are different to pcre regex classes - you may have seen regexes with things like `\w` for alphanumeric characters and `\s` for space characters.  sed uses posix character classes like `[[:alnum:]]` and `[[:space:]]`, respectively

also note that a lot of languages use `$1` syntax to identify matchgroups, sed uses escape-match

using the earlier example, we could adapt what we had to extract the filename using character classes instead

`find . -type f | sed -r 's/.*\/([[:alnum:]]+)\.([[:alnum:]]+)$/\1, \2/'`

## writing to a file and backups: replace in-place

with backups:

`sed -i.bak 's/{{name}}/devops dojo' example2.txt`

## stuff we didn't even get a chance to tuoch

* `sed` supports scripting
* the hold buffer (`-h`)
* match addressing
* multiple expression syntax

## for a pretty complete writeup...

http://www.grymoire.com/Unix/Sed.html
